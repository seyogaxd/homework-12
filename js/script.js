function createNewUser() {
    let userName = prompt("Youre name: ");
    let userSurname = prompt("Youre surname: ");

    const newUser = {
        firstName: userName,
        lastName: userSurname,
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        setFirstName(newName) {
            this.firstName = newName;
        },
        setLastName(newName) {
            this.lastName = newName;
        }
    };
    return newUser;
}
const user = createNewUser();
console.log(user.getLogin());

user.setFirstName("anotherName");
console.log(user.getLogin()); 